var direction = 0;
var isGameOver = false;
const MOVING_SPEED = 0.5;
const SCALING = 3;
window.addEventListener("DOMContentLoaded", function() {
  // get the canvas DOM element
  var canvas = document.getElementById("renderCanvas");

  let min = Math.min(window.innerWidth, window.innerHeight);
  canvas.width = min;
  canvas.height = min;
  canvas.style.marginTop = 100;

  // load the 3D engine
  var engine = new BABYLON.Engine(canvas, true);
  // createScene function that creates and return the scene
  var createScene = function(engine) {
    var scene = new BABYLON.Scene(engine);

    // Camera
    var camera = new BABYLON.ArcRotateCamera(
      "Camera",
      (3 * Math.PI) / 2,
      Math.PI / 2.5,
      50,
      BABYLON.Vector3.Zero(),
      scene
    );
    camera.attachControl(canvas, true);
    var cameraDirector = BABYLON.MeshBuilder.CreateBox(
      "cameraDirector",
      { size: 0.01 },
      scene
    );
    camera.target = cameraDirector;

    // Light
    var light = new BABYLON.HemisphericLight(
      "light1",
      new BABYLON.Vector3(0, 1, 0),
      scene
    );

    showAxis(20, scene);
    // drawGorilla(scene);

    createEnvironment(scene);
    // drawAngryBird(scene, 0, 10, 0, false);

    keyboardController(scene);

    drawGorilla(scene);
    drawManyAngryBirds(scene);

    console.log("woods", scene.getMeshByID("WoodParent")._children.length);

    // var woods = scene.getMeshByID("WoodParent")._children;
    // scene.registerBeforeRender(function() {
    //   if (isGameOver) {
    //     return;
    //   }
    //   if (skull !== null && skull !== undefined) {
    //     if (skull.position.z < 1000) {
    //       skull.position.z += MOVING_SPEED;
    //       cameraDirector.position.z += MOVING_SPEED;
    //     }

    //     turnGorilla(skull, direction);

    //     for (let index = 0; index < woods.length; index++) {
    //       const w = woods[index];

    //       if (w.intersectsMesh(skull, false)) {
    //         console.log("Game Over ffffffukkkk", index);
    //         isGameOver = true;
    //         break;
    //       } else {
    //       }
    //     }
    //   }
    // });

    return scene;
  };
  // call the createScene function
  var scene = createScene();
  // run the render loop
  engine.runRenderLoop(function() {
    scene.render();
  });
  // the canvas/window resize event handler
  window.addEventListener("resize", function() {
    engine.resize();
  });
});

//Local Axes
function localAxes(size, scene) {
  var pilot_local_axisX = BABYLON.Mesh.CreateLines(
    "pilot_local_axisX",
    [
      new BABYLON.Vector3.Zero(),
      new BABYLON.Vector3(size, 0, 0),
      new BABYLON.Vector3(size * 0.95, 0.05 * size, 0),
      new BABYLON.Vector3(size, 0, 0),
      new BABYLON.Vector3(size * 0.95, -0.05 * size, 0)
    ],
    scene
  );
  pilot_local_axisX.color = new BABYLON.Color3(1, 0, 0);

  pilot_local_axisY = BABYLON.Mesh.CreateLines(
    "pilot_local_axisY",
    [
      new BABYLON.Vector3.Zero(),
      new BABYLON.Vector3(0, size, 0),
      new BABYLON.Vector3(-0.05 * size, size * 0.95, 0),
      new BABYLON.Vector3(0, size, 0),
      new BABYLON.Vector3(0.05 * size, size * 0.95, 0)
    ],
    scene
  );
  pilot_local_axisY.color = new BABYLON.Color3(0, 1, 0);

  var pilot_local_axisZ = BABYLON.Mesh.CreateLines(
    "pilot_local_axisZ",
    [
      new BABYLON.Vector3.Zero(),
      new BABYLON.Vector3(0, 0, size),
      new BABYLON.Vector3(0, -0.05 * size, size * 0.95),
      new BABYLON.Vector3(0, 0, size),
      new BABYLON.Vector3(0, 0.05 * size, size * 0.95)
    ],
    scene
  );
  pilot_local_axisZ.color = new BABYLON.Color3(0, 0, 1);

  var local_origin = BABYLON.MeshBuilder.CreateBox(
    "local_origin",
    { size: 1 },
    scene
  );
  local_origin.isVisible = false;

  pilot_local_axisX.parent = local_origin;
  pilot_local_axisY.parent = local_origin;
  pilot_local_axisZ.parent = local_origin;

  return local_origin;
}

// show axis
var showAxis = function(size, scene) {
  var makeTextPlane = function(text, color, size) {
    var dynamicTexture = new BABYLON.DynamicTexture(
      "DynamicTexture",
      50,
      scene,
      true
    );
    dynamicTexture.hasAlpha = true;
    dynamicTexture.drawText(
      text,
      5,
      40,
      "bold 36px Arial",
      color,
      "transparent",
      true
    );
    var plane = new BABYLON.Mesh.CreatePlane("TextPlane", size, scene, true);
    plane.material = new BABYLON.StandardMaterial("TextPlaneMaterial", scene);
    plane.material.backFaceCulling = false;
    plane.material.specularColor = new BABYLON.Color3(0, 0, 0);
    plane.material.diffuseTexture = dynamicTexture;
    return plane;
  };

  var axisX = BABYLON.Mesh.CreateLines(
    "axisX",
    [
      new BABYLON.Vector3.Zero(),
      new BABYLON.Vector3(size, 0, 0),
      new BABYLON.Vector3(size * 0.95, 0.05 * size, 0),
      new BABYLON.Vector3(size, 0, 0),
      new BABYLON.Vector3(size * 0.95, -0.05 * size, 0)
    ],
    scene
  );
  axisX.color = new BABYLON.Color3(1, 0, 0);
  var xChar = makeTextPlane("X", "red", size / 10);
  xChar.position = new BABYLON.Vector3(0.9 * size, -0.05 * size, 0);
  var axisY = BABYLON.Mesh.CreateLines(
    "axisY",
    [
      new BABYLON.Vector3.Zero(),
      new BABYLON.Vector3(0, size, 0),
      new BABYLON.Vector3(-0.05 * size, size * 0.95, 0),
      new BABYLON.Vector3(0, size, 0),
      new BABYLON.Vector3(0.05 * size, size * 0.95, 0)
    ],
    scene
  );
  axisY.color = new BABYLON.Color3(0, 1, 0);
  var yChar = makeTextPlane("Y", "green", size / 10);
  yChar.position = new BABYLON.Vector3(0, 0.9 * size, -0.05 * size);
  var axisZ = BABYLON.Mesh.CreateLines(
    "axisZ",
    [
      new BABYLON.Vector3.Zero(),
      new BABYLON.Vector3(0, 0, size),
      new BABYLON.Vector3(0, -0.05 * size, size * 0.95),
      new BABYLON.Vector3(0, 0, size),
      new BABYLON.Vector3(0, 0.05 * size, size * 0.95)
    ],
    scene
  );
  axisZ.color = new BABYLON.Color3(0, 0, 1);
  var zChar = makeTextPlane("Z", "blue", size / 10);
  zChar.position = new BABYLON.Vector3(0, 0.05 * size, 0.9 * size);
};

var drawGorilla = function(scene) {
  var gorilla;
  var gorillaHeaderPoint = BABYLON.MeshBuilder.CreateBox(
    "gorillaHeaderPoint",
    { size: 1 / SCALING },
    scene
  );

  var gorillaLeftHandPoint = BABYLON.MeshBuilder.CreateBox(
    "gorillaLeftHandPoint",
    { size: 1 / SCALING },
    scene
  );

  var gorillaRightHandPoint = BABYLON.MeshBuilder.CreateBox(
    "gorillaRightHandPoint",
    { size: 1 / SCALING },
    scene
  );

  // The first parameter can be used to specify which mesh to import. Here we import all meshes
  BABYLON.SceneLoader.ImportMesh("", "./", "./gorilla.obj", scene, function(
    newMeshes
  ) {
    // // Set the target of the camera to the first imported mesh
    // // camera.target = newMeshes[0];

    console.log("newMeshes size");

    gorillaModel = newMeshes[0];
    gorillaModel.showBoundingBox = true;

    //======== Setup Head Box position ============
    gorillaHeaderPoint.position.y =
      getDimensions(gorillaModel).height -
      getDimensions(gorillaHeaderPoint).height;
    //=============================================

    //======== Setup Head Box position ============
    gorillaLeftHandPoint.position.y = getDimensions(gorillaModel).height / 2;
    gorillaLeftHandPoint.position.x = -getDimensions(gorillaModel).width / 2;

    gorillaRightHandPoint.position.y = getDimensions(gorillaModel).height / 2;
    gorillaRightHandPoint.position.x = getDimensions(gorillaModel).width / 2;
    //=============================================

    gorilla = BABYLON.Mesh.MergeMeshes([
      newMeshes[0],
      gorillaHeaderPoint,
      gorillaLeftHandPoint,
      gorillaRightHandPoint
    ]);
    var scalingFactor = new BABYLON.Vector3(SCALING, SCALING, SCALING);
    gorilla.scaling = scalingFactor;
    gorilla.position.multiplyInPlace(scalingFactor);

    gorilla.position.x = 0;
    gorilla.position.y = 5;
    gorilla.position.z = 0;
    gorilla.showBoundingBox = true;

    gorillaModel.rotation.x = -Math.PI / 2;
    gorillaModel.rotation.y = 0;
    gorillaModel.rotation.z = 0;

    turnGorilla(gorilla, 0);
  });

  var woods = scene.getMeshByID("WoodParent")._children;
  scene.registerBeforeRender(function() {
    if (isGameOver) {
      return;
    }
    if (gorilla !== null && gorilla !== undefined) {
      if (gorilla.position.z < 1000) {
        gorilla.position.z += MOVING_SPEED;
        scene.getMeshByID("cameraDirector").position.z += MOVING_SPEED;
      }

      turnGorilla(gorilla, direction);

      for (let index = 0; index < woods.length; index++) {
        const w = woods[index];

        if (w.intersectsMesh(gorilla, false)) {
          console.log("Game Over ffffffukkkk", index);
          isGameOver = true;
          break;
        } else {
        }
      }
    }
  });
};

function turnGorilla(gorilla, dir) {
  switch (dir) {
    case 0:
      gorilla.position.x = 0;

      gorilla.rotation.x = Math.PI / 2;
      gorilla.rotation.y = 0;
      gorilla.rotation.z = 0;

      break;
    case 1:
      gorilla.position.x = -5;

      gorilla.rotation.x = Math.PI / 4;
      gorilla.rotation.y = Math.PI / 2;
      gorilla.rotation.z = Math.PI / 2;

      // gorilla.rotate(BABYLON.Axis.Z, Math.PI / 4, BABYLON.Space.WORLD);
      break;
    case 2:
      gorilla.position.x = 5;

      gorilla.rotation.x = (3 * Math.PI) / 4;
      gorilla.rotation.y = Math.PI / 2;
      gorilla.rotation.z = Math.PI / 2;

      // gorilla.rotate(BABYLON.Axis.Z, -Math.PI / 4, BABYLON.Space.WORLD);
      break;

    default:
      break;
  }
}

var drawRocks = function(scene) {
  var cone = BABYLON.MeshBuilder.CreateCylinder(
    "cone",
    { diameterTop: 5, diameterBottom: 5, height: 500, tessellation: 500 },
    scene
  );
  cone.rotation.x = Math.PI / 2;
  cone.rotation.y = 0;
  cone.rotation.z = 0;

  cone.position.x = 10;
  cone.position.y = 5;
  cone.position.z = 500 / 2;

  var cone2 = BABYLON.MeshBuilder.CreateCylinder(
    "cone2",
    { diameterTop: 5, diameterBottom: 5, height: 500, tessellation: 500 },
    scene
  );
  cone2.rotation.x = Math.PI / 2;
  cone2.rotation.y = 0;
  cone2.rotation.z = 0;

  cone2.position.x = -10;
  cone2.position.y = 5;
  cone2.position.z = 500 / 2;
};

var drawManyWoods = function(scene) {
  var woodParent = BABYLON.Mesh.CreateBox("WoodParent", {}, scene);
  for (let index = 1; index < 10; index++) {
    drawWood(index * 100, scene);
  }
};

var drawWood = function(zPos, scene) {
  // Sphere
  var woodMaterial = new BABYLON.StandardMaterial("sphereMaterial", scene);
  woodMaterial.diffuseTexture = new BABYLON.Texture(
    "//www.babylonjs.com/assets/wood.jpg",
    scene
  );

  var wood = BABYLON.MeshBuilder.CreateCylinder(
    `wood-${zPos}`,
    { diameterTop: 3, diameterBottom: 3, height: 30, tessellation: 500 },
    scene
  );
  // wood.rotation.x = Math.PI / 2;
  // wood.rotation.y = Math.PI / 2;
  wood.rotation.z = Math.PI / 2;
  wood.position.z = zPos;
  wood.material = woodMaterial;
  wood.parent = scene.getMeshByID("WoodParent");
  // wood.showBoundingBox = true;

  let waterMaterial = scene.getMaterialByName("waterMaterial");

  scene.registerBeforeRender(function() {
    let time = waterMaterial._lastTime / 100000;
    let x = wood.position.x;
    let z = wood.position.z;
    wood.position.y = Math.abs(
      Math.sin(x / 0.05 + time * waterMaterial.waveSpeed) *
        waterMaterial.waveHeight *
        waterMaterial.windDirection.x *
        5.0 +
        Math.cos(z / 0.05 + time * waterMaterial.waveSpeed) *
          waterMaterial.waveHeight *
          waterMaterial.windDirection.y *
          5.0
    );
  });

  var frameRate = 0.5;
  //Rotation Animation
  var yRot = new BABYLON.Animation(
    "yRot",
    "rotation.x",
    frameRate,
    BABYLON.Animation.ANIMATIONTYPE_FLOAT,
    BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
  );

  var keyFramesR = [];

  keyFramesR.push({
    frame: 0,
    value: 0
  });

  keyFramesR.push({
    frame: frameRate,
    value: Math.PI
  });

  keyFramesR.push({
    frame: 2 * frameRate,
    value: 2 * Math.PI
  });

  yRot.setKeys(keyFramesR);

  scene.beginDirectAnimation(wood, [yRot], 0, 2 * frameRate, true);

  return wood;
};

var drawManyAngryBirds = function(scene) {
  var AngryBirdParent = BABYLON.Mesh.CreateBox("AngryBirdParent", {}, scene);
  for (let index = 0; index < 5; index++) {
    drawAngryBird(scene, 10 % index, 10, 50 * index, index % 2 === 0);
  }
};

var drawAngryBird = function(scene, x, y, z, isCrazyHotizontal) {
  var yellowMat = new BABYLON.StandardMaterial("yellowMat", scene);
  yellowMat.diffuseColor = new BABYLON.Color3(0.93, 1, 0);

  var redMat = new BABYLON.StandardMaterial("redMat", scene);
  redMat.diffuseColor = new BABYLON.Color3(1, 0, 0);

  var body = BABYLON.MeshBuilder.CreateBox(
    "body",
    { height: 5, width: 3, depth: 1.5 },
    scene
  );
  body.position = new BABYLON.Vector3(x, y, z);
  body.material = yellowMat;

  var leftWing = BABYLON.MeshBuilder.CreateBox(
    "leftWing",
    { height: 1.8, width: 0.5, depth: 1.5 },
    scene
  );

  leftWing.position = new BABYLON.Vector3(
    -(getDimensions(body).width + getDimensions(leftWing).width) / 2 + x,
    y + 0.5,
    z
  );
  leftWing.material = redMat;

  var rightWing = BABYLON.MeshBuilder.CreateBox(
    "rightWing",
    { height: 1.8, width: 0.5, depth: 1.5 },
    scene
  );
  rightWing.position = new BABYLON.Vector3(
    (getDimensions(body).width + getDimensions(leftWing).width) / 2 + x,
    y + 0.5,
    z
  );
  rightWing.material = redMat;

  var creatLeg = function(x, y, z, name) {
    var legMat = new BABYLON.StandardMaterial("legMat", scene);
    legMat.diffuseColor = new BABYLON.Color3(1, 0.68, 0);

    var leg = BABYLON.MeshBuilder.CreateBox(
      name,
      { height: 1, width: 0.5, depth: 0.5 },
      scene
    );
    leg.position = new BABYLON.Vector3(x, y, z);
    leg.material = legMat;

    var nail1 = BABYLON.MeshBuilder.CreateBox(
      `nail1-${name}`,
      { height: 0.5, width: 1.5, depth: 0.5 },
      scene
    );
    nail1.position = new BABYLON.Vector3(
      x,
      y - (getDimensions(leg).width + getDimensions(nail1).height) / 2,
      z
    );
    nail1.material = legMat;

    var nail2 = BABYLON.MeshBuilder.CreateBox(
      `nail2-${name}`,
      { height: 0.5, width: 0.5, depth: 1.5 },
      scene
    );
    nail2.position = new BABYLON.Vector3(
      x - 0.5,
      y - (getDimensions(leg).width + getDimensions(nail2).height) / 2,
      z
    );
    nail2.material = legMat;

    var nail3 = BABYLON.MeshBuilder.CreateBox(
      `nail3-${name}`,
      { height: 0.5, width: 0.5, depth: 1.5 },
      scene
    );
    nail3.position = new BABYLON.Vector3(
      x + 0.5,
      y - (getDimensions(leg).width + getDimensions(nail2).height) / 2,
      z
    );
    nail3.material = legMat;

    return BABYLON.Mesh.MergeMeshes([leg, nail1, nail2, nail3]);
  };

  var leftLeg = creatLeg(
    x - ((getDimensions(body).width / 2) * 2) / 3,
    y - getDimensions(body).height / 2 - 1 / 2,
    z,
    "leftLeg"
  );
  var rightLeg = creatLeg(
    x + ((getDimensions(body).width / 2) * 2) / 3,
    y - getDimensions(body).height / 2 - 1 / 2,
    z,
    "rightLeg"
  );

  var creatFace = function() {
    var nose = BABYLON.MeshBuilder.CreateBox("nose", { size: 0.5 }, scene);

    nose.position = new BABYLON.Vector3(x, y + 1, z - 0.5 * 2);
    nose.material = redMat;

    var eyeMat = new BABYLON.StandardMaterial("eyeMat", scene);
    eyeMat.diffuseColor = new BABYLON.Color3(0, 0, 0);

    var leftEye = BABYLON.MeshBuilder.CreateBox(
      "leftEye",
      { size: 0.5, depth: 0.05 },
      scene
    );

    leftEye.position = new BABYLON.Vector3(x - 0.5, y + 1.5, z - 0.5 * 2);
    leftEye.material = eyeMat;

    var rightEye = BABYLON.MeshBuilder.CreateBox(
      "rightEye",
      { size: 0.5, depth: 0.05 },
      scene
    );

    rightEye.position = new BABYLON.Vector3(x + 0.5, y + 1.5, z - 0.5 * 2);
    rightEye.material = eyeMat;

    return BABYLON.Mesh.MergeMeshes(
      [nose, leftEye, rightEye],
      true,
      true,
      undefined,
      false,
      true
    );
  };

  var face = creatFace();

  var angryBird = BABYLON.Mesh.MergeMeshes(
    [face, body, leftWing, rightWing, leftLeg, rightLeg],
    true,
    true,
    undefined,
    false,
    true
  );
  angryBird.showBoundingBox = true;
  // angryBird.parent = scene.getMeshByID("WoodParent");
  //Vertical Position Animation
  var frameRate = 10;
  var ySlide = new BABYLON.Animation(
    "ySlide",
    "position.y",
    frameRate,
    BABYLON.Animation.ANIMATIONTYPE_FLOAT,
    BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
  );

  var keyFramesP = [];

  keyFramesP.push({
    frame: 0,
    value: 1
  });

  keyFramesP.push({
    frame: frameRate,
    value: -1
  });

  keyFramesP.push({
    frame: 2 * frameRate,
    value: 1
  });
  ySlide.setKeys(keyFramesP);

  //Hotizontal Position Animation
  var xSlide = new BABYLON.Animation(
    "xSlide",
    "position.x",
    frameRate,
    BABYLON.Animation.ANIMATIONTYPE_FLOAT,
    BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
  );

  var keyFramesXSlide = [];

  keyFramesXSlide.push({
    frame: 0,
    value: 5
  });

  keyFramesXSlide.push({
    frame: frameRate,
    value: -5
  });

  keyFramesXSlide.push({
    frame: 2 * frameRate,
    value: 5
  });
  xSlide.setKeys(keyFramesXSlide);

  var array = [ySlide];

  if (isCrazyHotizontal) {
    array = [xSlide];
  }

  scene.beginDirectAnimation(angryBird, array, 0, 2 * frameRate, true);
};

var getDimensions = function(mesh) {
  if (mesh === null || mesh === undefined) return null;

  var vectorsWorld = mesh.getBoundingInfo().boundingBox.vectorsWorld;

  return {
    height: Number(vectorsWorld[1].y - vectorsWorld[0].y),
    width: Number(vectorsWorld[1].x - vectorsWorld[0].x),
    depth: Number(vectorsWorld[1].z - vectorsWorld[0].z)
  };
};

var keyboardController = function(scene) {
  // Keyboard events
  var inputMap = {};
  scene.actionManager = new BABYLON.ActionManager(scene);
  scene.actionManager.registerAction(
    new BABYLON.ExecuteCodeAction(
      BABYLON.ActionManager.OnKeyDownTrigger,
      function(evt) {
        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
      }
    )
  );
  scene.actionManager.registerAction(
    new BABYLON.ExecuteCodeAction(
      BABYLON.ActionManager.OnKeyUpTrigger,
      function(evt) {
        inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
      }
    )
  );

  // Game/Render loop
  scene.onBeforeRenderObservable.add(() => {
    if (inputMap["a"] || inputMap["ArrowLeft"]) {
      direction = 1;
      console.log("aaa");
    } else if (inputMap["d"] || inputMap["ArrowRight"]) {
      direction = 2;
      console.log("ddd");
    } else {
      direction = 0;
    }
  });
};

var createEnvironment = function(scene) {
  // Skybox
  var skybox = BABYLON.Mesh.CreateBox("skyBox", 5000.0, scene);
  var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
  skyboxMaterial.backFaceCulling = false;
  skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(
    "//www.babylonjs.com/assets/skybox/TropicalSunnyDay",
    scene
  );
  skyboxMaterial.reflectionTexture.coordinatesMode =
    BABYLON.Texture.SKYBOX_MODE;
  skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
  skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
  skyboxMaterial.disableLighting = true;
  skybox.material = skyboxMaterial;

  // Water material
  var waterMaterial = new BABYLON.WaterMaterial(
    "waterMaterial",
    scene,
    new BABYLON.Vector2(512, 512)
  );
  waterMaterial.bumpTexture = new BABYLON.Texture(
    "//www.babylonjs.com/assets/waterbump.png",
    scene
  );
  waterMaterial.windForce = -10;
  waterMaterial.waveHeight = 0.1;
  waterMaterial.bumpHeight = 0.1;
  waterMaterial.waveLength = 0.1;
  waterMaterial.waveSpeed = 50.0;
  waterMaterial.colorBlendFactor = 0;
  waterMaterial.windDirection = new BABYLON.Vector2(1, 1);
  waterMaterial.colorBlendFactor = 0;

  // Ground
  var groundTexture = new BABYLON.Texture(
    "//www.babylonjs.com/assets/sand.jpg",
    scene
  );
  groundTexture.vScale = groundTexture.uScale = 4.0;

  var groundMaterial = new BABYLON.StandardMaterial("groundMaterial", scene);
  groundMaterial.diffuseTexture = groundTexture;

  var ground = BABYLON.Mesh.CreateGround("ground", 512, 512, 32, scene, false);
  ground.position.y = -1;
  ground.material = groundMaterial;

  // Water mesh
  var waterMesh = BABYLON.Mesh.CreateGround(
    "waterMesh",
    512,
    512,
    32,
    scene,
    false
  );
  waterMesh.material = waterMaterial;

  drawManyWoods(scene);

  // Configure water material
  waterMaterial.addToRenderList(ground);
  waterMaterial.addToRenderList(skybox);
};
